﻿param
(
	$ComputerName
)
$action = {
	foreach ($item in (Get-Content 'C:\Zabbix\conf\zabbix_agent2.win.conf')) {
		$splited = $item -split '='
		[PscustomObject]@{
			Name = $splited[0]
			Value = $splited[1]
		}
	}
}

if ($ComputerName) {
	Invoke-Command -ComputerName $ComputerName -ScriptBlock $action
}
else {
	Invoke-Command -ScriptBlock $action
}