﻿param
(
	$ComputerName
)
$action = {
	$Config = Get-Content 'C:\zabbix\conf\RegularParams.conf'
	if ([string]::IsNullOrWhiteSpace($Config)) {
		Write-Error "RegularParams.conf is empty"
		return
	}
	else {
		$Config = $Config.trim()
		$Name = ($Config | Select-String '#(?!.* ).*' -AllMatches).Matches.Value
		foreach ($item in $Name) {
			$NextItemValue = $Name[$Name.IndexOf($item) + 1]
			if ($Config.IndexOf($NextItemValue) -ne -1) {
				$NextStrIndex = ($Config.IndexOf($NextItemValue) - 1)
			}
			else {
				$NextStrIndex = $Config.length
			}
			[pscustomobject]@{
				Name = $item -replace '#'
				UserParameter = $Config[($Config.IndexOf($item) + 1) .. $NextStrIndex] | Out-String
			}
		}
	}
	
}
if ($ComputerName) {
	Invoke-Command -ComputerName $ComputerName -ScriptBlock $action
}
else {
	Invoke-Command -ScriptBlock $action
}