[CmdletBinding()]
param
(
	$ComputerName,
	$Name,
	$UserParameter,
	$UserParameterPath = 'C:\zabbix\conf\RegularParams.conf',
	[ValidateSet('Replace', 'Add')]
	$Method = 'Replace'
)
$action = {
	param
	(
		$UserParameter,
		$UserParamsPath,
		$Method
	)
	function ConvertTo-TextUserParameter {
		param
		(
			$UserParameter
		)
		$sb = [System.Text.StringBuilder]::new()
		$null = foreach ($item in $UserParameter) {
			$sb.AppendLine("#$($item.name)")
			$sb.AppendLine($item.UserParameter)
		}
		$sb.ToString()
	}
	function Get-ZaUserParameter {
		param
		(
			[parameter(Mandatory = $true)]
			$UserParamsPath
		)
		$Config = Get-Content -Path $UserParamsPath
		If ([string]::IsNullOrWhiteSpace($Config)) {
			return
		}
		$Config = $Config.trim()
		$Name = ($Config | Select-String '#(?!.* ).*' -AllMatches).Matches.Value
		foreach ($item in $Name) {
			$NextItemValue = $Name[$Name.IndexOf($item) + 1]
			if ($Config.IndexOf($NextItemValue) -ne -1) {
				$NextStrIndex = ($Config.IndexOf($NextItemValue) - 1)
			}
			else {
				$NextStrIndex = $Config.length - 1
			}
			$UserParameter = $Config[($Config.IndexOf($item) + 1) .. $NextStrIndex] | Where-Object { $_ -ne '' }
			[pscustomobject]@{
				Name = $item -replace '#'
				UserParameter = ($UserParameter | Out-String).Trim()
			}
		}
	}
	[pscustomobject[]]$CurentParams = Get-ZaUserParameter -UserParamsPath $UserParamsPath
	if (-not $CurentParams) {
		$CurentParams = $UserParameter
	}
	if (-not ($CurentParams | Where-Object Name -Like $UserParameter.Name)) {
		$CurentParams += $UserParameter
	}
	
	else {
		$TargetObject = $CurentParams | Where-Object {
			$_.name -Like $UserParameter.Name -and
			$_.UserParameter -contains $UserParameter.UserParameter
		}
	
		switch ($Method) {
			Replace {
				$TargetObject.UserParameter = $UserParameter.UserParameter
			}
			Add {
				foreach ($item in $UserParameter.UserParameter) {
					if ($TargetObject.UserParameter -notcontains $item) {
						$TargetObject.UserParameter += "`n$item"
					}
				}
			}
		}
	}
	$Text = ConvertTo-TextUserParameter -UserParameter ($CurentParams | Sort-Object Name)
	$Text | Out-File -FilePath $UserParamsPath -Encoding default
	
<#	$Params = New-Object System.Collections.ArrayList $null
	$paramGetZaUserParameter = @{
		UserParamsPath = $UserParamsPath
	}
	$CurentParams = Get-ZaUserParameter @paramGetZaUserParameter |
	Where-Object Name -NotLike $UserParameter.Name
	if ($CurentParams) {
		if ($CurentParams.Count -gt 1) {
			[void]$Params.AddRange($CurentParams)
		}
		else {
			[void]$Params.Add($CurentParams)
		}
	}
	[void]$Params.AddRange($UserParameter)
	$TextParams = ConvertTo-TextUserParameter -UserParameter ($Params | Sort-Object Name)
	$TextParams | Out-File -FilePath $UserParamsPath -Encoding default#>
}
$UserParameter = [pscustomobject]@{
	Name		  = $Name
	UserParameter = $UserParameter -split '\n'
}
if ($ComputerName) {
	Invoke-Command -ComputerName $ComputerName -ScriptBlock $action -ArgumentList $UserParameter, $UserParameterPath, $Method
}
else {
	Invoke-Command -ScriptBlock $action -ArgumentList $UserParameter, $UserParameterPath, $Method
}