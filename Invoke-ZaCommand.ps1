﻿
[CmdletBinding()]
param
(
	[string]$ZabbixAgentPath = 'C:\Zabbix\bin\zabbix_agent2.exe',
	[string[]]$ComputerName,
	[string]$Command
)
$Action = {
	param
	(
		$ZabbixAgentPath = 'C:\Zabbix\bin\zabbix_agent2.exe',
		[string]$Command,
		$InnerErrorActionPreference,
		$Verbose
	)
	$VerbosePreference = $Verbose
	$ErrorActionPreference = 'Continue'
	try {
		$StdOut = Invoke-Expression -Command "$ZabbixAgentPath $Command" -ErrorVariable ZabbixOut *>&1
	}
	catch {
		$PSCmdlet.ThrowTerminatingError($_)
	}
	if ($Lastexitcode -eq 0) {
		if ($ZabbixOut.Exception.Message) {
			Write-Verbose -Message ("$($Env:COMPUTERNAME): $([string]$ZabbixOut.Exception.Message)")
		}
		else {
			[pscustomobject]@{
				StdOut = $StdOut
			}
		}
	}
	else {
		$ErrorActionPreference = $InnerErrorActionPreference
		$ZabbixOut | Write-Error
	}
}
$ArgumentList = $ZabbixAgentPath, $Command, $ErrorActionPreference, $VerbosePreference
if ($ComputerName) {
	Invoke-Command -ScriptBlock $Action -ComputerName $ComputerName -ArgumentList $ArgumentList
}
else {
	Invoke-Command -ScriptBlock $Action -ArgumentList $ArgumentList
}