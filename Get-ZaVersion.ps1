﻿[CmdletBinding()]
param
(
	[string[]]$ComputerName,
	[string]$ZabbixAgentPath = "C:\Zabbix\bin\zabbix_agentd.exe",
	[switch]$FromAgent
)
function Get-ZaVersionFromAgent {
	function Get-ZaAgentCompilationTime {
		param
		(
			[string]$ZabbixVersionInfo
		)
		$DatePattern = '(?<=compilation time: )\w\w\w\ \d\d\ \d\d\d\d\ \d\d:\d\d:\d\d'
		$CompilationTimeString = ($ZabbixVersionInfo | Select-String -Pattern $DatePattern).Matches.Value
		Write-Debug "CompilationTimeString: $CompilationTimeString"
		[datetime]::ParseExact($CompilationTimeString, 'MMM dd yyyy HH:mm:ss', [System.Globalization.CultureInfo]::new("en-US"))
	}
	
	$Out = Invoke-ZaCommand -Command '-V' -ComputerName $ComputerName -ZabbixAgentPath $ZabbixAgentPath
	foreach ($Item in $Out) {
		[pscustomobject]@{
			Version = ($Item.StdOut | Select-String -Pattern '(?<=\(Zabbix\)\ ).*?(?=,|\ |$)').Matches.Value
			Revision = ($Item.StdOut | Select-String -Pattern '(?<=Revision ).*?(?=,|\ |$)').Matches.Value
			CompilationTime = Get-ZaAgentCompilationTime -ZabbixVersionInfo $Item.StdOut
			ComputerName = $Item.PsComputerName
		}
	}
}
if (-not $FromAgent) {
	$Action = {
		param ($ZabbixAgentPath)
		(Get-ChildItem $ZabbixAgentPath).VersionInfo
	}
	$ArgumentList = $ZabbixAgentPath
	if ($ComputerName) {
		Invoke-Command -ScriptBlock $Action -ComputerName $ComputerName -ArgumentList $ArgumentList
	}
	else {
		Invoke-Command -ScriptBlock $Action -ArgumentList $ArgumentList
	}
}
else {
	Get-ZaVersionFromAgent
}