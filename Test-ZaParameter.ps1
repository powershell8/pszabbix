﻿[CmdletBinding()]
param
(
	[Alias('Discovery', 'Lld', 'ItemKey')]
	$Parameter,
	$ComputerName,
	$ConfigName = 'zabbix_agent2.win.conf',
	[switch]$OutAsPlainText,
	[switch]$OutAsRaw
)
function Extract-ZaValue {
	param
	(
		$StdOut
	)
	if ($OutAsRaw) {
		[pscustomobject] @{
			Value = $StdOut.StdOut
		}
		continue
	}
	$Extracted = ([string]$StdOut.StdOut | Select-String -Pattern '(?<=\[[a-z]\|).*(?=])' -AllMatches).Matches.Value
	if ($OutAsPlainText) {
		[pscustomobject] @{
			Value = $Extracted
		}
		continue
	}
	switch -regex ($Extracted) {
		'ZBX_NOTSUPPORTED' {
			Write-Error -Message "Unsupported item key $Parameter"
			break
		}
		'{.*}' {
			#(?<="){#|}(?=") remove zabbix macros notation {#MACRONAME}
			$switch.Current -replace '(?<="){#|}(?=")' | ConvertFrom-Json
		}
		default {
			[pscustomobject] @{
				Value = $switch.Current
			}
		}
	}
}

$AliasValue = 'C:\Users\J.Kilroy\Documents\GitHub\kilroy_sandbox\Ps\PowerShell Studio\Files\Zabbix\Invoke-ZaCommand.ps1'
New-Alias -Name 'Invoke-ZaCommand' -Value $AliasValue -ErrorAction SilentlyContinue
$paramInvokeZaCommand = @{
	ComputerName = $ComputerName
	Command	     = "-t '$Parameter' --config 'c:\zabbix\conf\$ConfigName'"
}
Write-Verbose "Get param $Parameter"
foreach ($item in (Invoke-ZaCommand @paramInvokeZaCommand)) {
	$paramAddMember = @{
		NotePropertyName  = 'ComputerName'
		NotePropertyValue = $item.PSComputerName
		PassThru		  = $true
	}
	Extract-ZaValue -StdOut $item | Add-Member @paramAddMember
}